import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private httpClient: HttpClient) { }

  private baseURL = 'http://localhost:8080/produits';
  public getAllProducts(): Observable<any> {
    return this.httpClient.get(this.baseURL);
  }
  
  //post prend url et objet
  public saveProduit(produit: any): Observable<any> {
    return this.httpClient.post(this.baseURL, produit);
  }

  public deleteProduit(id:number) : Observable<any>{
    return this.httpClient.delete(this.baseURL+"/"+id);
  }

  public getProduit(id:number):Observable<any>{
    return this.httpClient.get(this.baseURL+"/"+id);
  }

  public updateProduct(id:number, produit:any):Observable<any>{
    return this.httpClient.put(this.baseURL+"/"+id, produit);
  }
}
