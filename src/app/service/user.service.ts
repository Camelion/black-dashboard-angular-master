import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }
  private baseURL = 'http://localhost:8080/users';
  public getAllUtilisateur(): Observable<any> {
    return this.httpClient.get(this.baseURL);
  }

  //post prend url et objet
  public saveUtilisateur(user: any): Observable<any> {
    return this.httpClient.post(this.baseURL, user);
  }

  public deleteUtilisateur(id:number) : Observable<any>{
    return this.httpClient.delete(this.baseURL+"/"+id);
  }


  public getUtilisateur(id:number):Observable<any>{
    return this.httpClient.get(this.baseURL+"/"+id);
  }

  public updateUtilisateur(id:number, user:any):Observable<any>{
    return this.httpClient.put(this.baseURL+"/"+id, user);
  }




}
