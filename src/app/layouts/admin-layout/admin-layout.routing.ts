import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { IconsComponent } from "../../pages/icons/icons.component";
import { MapComponent } from "../../pages/map/map.component";
import { NotificationsComponent } from "../../pages/notifications/notifications.component";
import { UserComponent } from "../../pages/user/user.component";
import { TablesComponent } from "../../pages/tables/tables.component";
import { TypographyComponent } from "../../pages/typography/typography.component";
import { EditUserComponent } from 'src/app/pages/edit-user/edit-user.component';
import { EditProductComponent } from 'src/app/pages/edit-product/edit-product.component';
import { ProduitComponent } from 'src/app/pages/produit/produit.component';
// import { RtlComponent } from "../../pages/rtl/rtl.component";

export const AdminLayoutRoutes: Routes = [
  // { path: "dashboard", component: DashboardComponent },
  // { path: "icons", component: IconsComponent },
  // { path: "maps", component: MapComponent },
  // { path: "notifications", component: NotificationsComponent },
  { path: "user", component: UserComponent },
  { path: "product", component: ProduitComponent },
  { path : "editUser/:id", component: EditUserComponent  },
  { path : "editProduct/:id", component: EditProductComponent  }
  // { path: "tables", component: TablesComponent },
  // { path: "typography", component: TypographyComponent },
  // { path: "rtl", component: RtlComponent }
];
