import { Component, OnInit } from "@angular/core";
import { Utilisateur } from 'src/app/model/utilisateur';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-user",
  templateUrl: "user.component.html"
})
export class UserComponent implements OnInit {
  users: any[];
  user : Utilisateur = new Utilisateur();
  constructor(private userService : UserService, private router: Router) {}

  ngOnInit() {
    this.chargerUtilisateur();
  }
  chargerUtilisateur(){
    this.userService.getAllUtilisateur().subscribe(data => {this.users = data});
    //variable data est affecté à l'utilisateur
  }

  saveUtilisateur() {
    this.userService.saveUtilisateur(this.user).subscribe(() => {
      this.chargerUtilisateur(); //charger la liste
      this.user = new Utilisateur(); // vider le formulaire
    });
    //premier probleme vider formulaire et mettre à jour
    //rafraichit tableau après utilisateur, puis vide formulaire afin de pouvoir s'en servir a nouveau


  }

  deleteUtilisateur(user){
    this.userService.deleteUtilisateur(user.idUtilisateur).subscribe(()=>{
      this.chargerUtilisateur();
    })
  }

  editUser(id:number){
    //exemple de url quand utilise la méthode
    //localhost:4200/editUser/4
    this.router.navigate(['editUser', id]);
  }
}
