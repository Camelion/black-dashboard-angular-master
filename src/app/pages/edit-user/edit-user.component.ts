import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { Utilisateur } from 'src/app/model/utilisateur';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  id:number;
  user:Utilisateur;
  constructor(private router:Router, private userService:UserService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.user=new Utilisateur();
    this.id=this.route.snapshot.params['id'] 
    //this route correspond àl'addresse précédente; ['id'] correspond a l'id de editUser/:id dans app-routing
    this.userService.getUtilisateur(this.id).subscribe(data => {
      this.user=data
    });

  }

  updateUtilisateur(){
    //remplit formulaire avec info du user ayant l'id voulu
    this.userService.updateUtilisateur(this.id, this.user).subscribe(
      data => console.log(data), error => console.log(error)
    );
    this.user = new Utilisateur();
    //retour vers la page /user
    this.router.navigate(['/user'])
  }

}
