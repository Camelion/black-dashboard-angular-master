import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/model/produit';
import { ActivatedRoute, Router } from '@angular/router';
import { ProduitService } from 'src/app/service/produit.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  id:number;
  produit:Produit;
  constructor(private router:Router, private produitService:ProduitService, private route:ActivatedRoute) { }


  ngOnInit(): void {
    this.produit = new Produit();
    this.id = this.route.snapshot.params['id']
    this.produitService.getProduit(this.id).subscribe(data =>{
      this.produit=data
    });
  }

  updateProduct(){
    this.produitService.updateProduct(this.id, this.produit).subscribe(
      data=> console.log(data), error => console.log(error)
    );
    this.produit = new Produit();
    this.router.navigate(['/product'])
  }

}
