import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/model/produit';
import { ProduitService } from 'src/app/service/produit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.scss']
})
export class ProduitComponent implements OnInit {
  products: any[];
  produit : Produit = new Produit();
  
  constructor(private produitService: ProduitService, private router: Router) { }

  ngOnInit(): void {
    this.chargerProduit();
  }
  chargerProduit(){
    this.produitService.getAllProducts().subscribe(data => {this.products=data} );
  }
  

  saveProduct() {
    this.produitService.saveProduit(this.produit).subscribe(( )=> {
      this.chargerProduit();
      this.produit = new Produit();
    });
    
  }

  deleteProduct(produit) {
    this.produitService.deleteProduit(produit.idProduit).subscribe(()=>{
      this.chargerProduit();
    })
  }

  editProduct(id:number){
    this.router.navigate(['editProduct', id]);
  }

}
